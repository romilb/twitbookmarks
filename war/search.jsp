<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.util.List" %>
<%@ page import="twitter4j.Status" %>
<%@ page import="twitter4j.Twitter" %>
<%@ page import="com.bookmarks.database.*" %>
<%@ page import="com.bookmarks.backend.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<%if(null== session.getAttribute("twitter")  || null!=session.getAttribute("requestToken") ) { session.invalidate();response.sendRedirect("index.jsp") ; }
else {
%>
<html>
<head>
<jsp:include page="header.jsp" />
<title>Twitmarks</title>
</head>
<body>
<div id="loggedin">   
    <div id="header">
		<a href="index.html" id="logo"> Twitmarks</a>

		<a href="./logout" class= "about"> Log Out </a>

	</div>
<%
Twitter twitter=(Twitter) session.getAttribute("twitter");
String tag=request.getParameter("tag");
tag= URLDecoder.decode(tag, "UTF-8");
TagDB tagdb=DBQueries.getTagObjectfromDB(twitter.getId(),tag);
%>
	<div id="container_feeds">
	
	 <% 
	 if(tagdb!=null) {
	 for(String bookmark:tagdb.getURLs()) {  %>
		<div class="row_feed"  >
			
			<div class="feed">
				<span> <a href="<%= bookmark%>" target="_blank"> <%=bookmark %>  </a></span>
			</div>

			
		</div>
	<% }  %>
	<% } else { %>
	<p> Sorry, No search results found </p>
	
	<%}%>
	</div>
</div>


</body>
</html>

<% } %>