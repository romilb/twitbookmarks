<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="twitter4j.Status" %>
<%@ page import="twitter4j.Twitter" %>
<%@ page import="com.bookmarks.database.*" %>
<%@ page import="com.bookmarks.backend.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<%if(null== session.getAttribute("twitter")  || null!=session.getAttribute("requestToken") ) { session.invalidate();response.sendRedirect("index.jsp") ; }
else {
%>
<html>
<head>
<link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="stylesheet.css" type="text/css"/>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.js"></script>
<script  type='text/javascript' src="js/main.js">
</script>
<title>Twitmarks</title>
</head>
<body>
<div id="loggedin">   
    <div id="header" >
		<a href="index.jsp" id="logo"> Twitmarks</a>
		<a href="./logout" class= "about"> Log Out </a>
		<a href="https://docs.google.com/spreadsheet/viewform?formkey=dHo4SVNKYW0yUmpuaHloeU1jR3h0WGc6MQ" target="_blank" class="about"> Contact Us </a>
	</div>
	<div id="header_notification" style="display:none">
       <p> Your book-marks are being updated in the background.</p>
    </div>
<%
Twitter twitter=(Twitter) session.getAttribute("twitter");
%>
	<div id="container_feeds">
	<div id="update_search">
		<a id="update_button"> Update Bookmarks from Twitter Feed</a>
		<input type="image" id = "search_button" src="images/lens.png" style="height:33px; "/>
		<input type="text" id="search" value="Search" onfocus="document.getElementById('search').value=''"/>
	</div>
	 <% 
	 List<BookmarkDB> forDisplay=DBQueries.getContentforDisplay(twitter.getId(),999999999999999999L);
	 //System.out.println(forDisplay);
	 for(int i=0;i<forDisplay.size();i++){
		 BookmarkDB books=forDisplay.get(i);
	 //for(BookmarkDB books:DBQueries.getContentforDisplay(twitter.getId()))  
	 %>
		<div class="row_feed" id=<%=books.getId() %> >
			<div class="edit">
				<% String tagsString=books.getEscapedTags().toString();
				   tagsString=  tagsString.substring(1,tagsString.length()-1);
				%>
				<span><img src="images/ico_edit.png" height="20" width="20" alt="Edit Bookmark" class="button1" onclick="edit('<%=books.getId()%>','<%=books.getbookmark()%>','<%=tagsString%>')"/></span>
				<span><img src="images/cross4.png" height="20" width="20" alt="Delete Bookmark" class="button2" onclick="deleteShow('<%= books.getId() %>')"/></span>
			</div>

			<div class="feed">
				<span> <a href="<%=books.getbookmark()%>" target="_blank"> <%=books.getbookmark().substring(0, Math.min(70,books.getbookmark().length())) %>...  </a></span>
			</div>

			<div class="tags">
			<% for(String s:books.getTags()) { %>
				<span><%= s %></span>
				<% } %>
			</div>
		</div>
	<% } %>
	</div>
</div>

<div id="modal_delete" style="display:none">

<div class="overlay"></div>
<div class="modal">
<div class="modal_header">
<p> Are you sure you want to delete the bookmarks?</p>

</div>
<div class="edit_cancel">
<input type="hidden" id="deleteid" value="id" />
<a href="#" onclick="javascript:document.getElementById('modal_delete').style.display='none'">Cancel</a>
<a href="#" onclick="javascript:deleteMark()">Delete</a>

</div>

</div>
</div>


<div id="modal_edit" style="display:none">

<div class="overlay"></div>
<div class="modal">
<div class="modal_header">
<p>Edit bookmark</p>

</div>


<div class="content" id="edit_values">
<input type="hidden" id="updateid" value="id" />
<input type="text" id="updateurl" value="url"/>
<input type="text" id="updatetags" value="tags"/>
</div>

<div class="edit_cancel">
<a href="#" onclick="javascript:document.getElementById('modal_edit').style.display='none'">Cancel</a>
<a href="#" onclick="javascript:save()">Save</a>


</div>

</div>
</div>

</body>
</html>

<% } %>
