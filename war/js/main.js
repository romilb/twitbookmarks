var updateRequested=false;
var listRequested=false;
$(document).ready(hoverchange);		
function hoverchange(){
	$(".button1").hover(function() {
		$(this).attr("src","images/ico_edit.png");
			}, function() {
		$(this).attr("src","images/ico_edit.png");
	});
	$(".button2").hover(function() {
		$(this).attr("src","images/cross3.png");
			}, function() {
		$(this).attr("src","images/cross4.png");
	});
}
function edit(id,bookmark,tag){
	document.getElementById('modal_edit').style.display='block';
	document.getElementById('updateid').value=id;
	document.getElementById('updateurl').value=bookmark;
	document.getElementById('updatetags').value=tag;
}
function deleteShow(id){
	document.getElementById('modal_delete').style.display='block';
	document.getElementById('deleteid').value=id;
}
function deleteMark(){
	var id = document.getElementById("deleteid").value;
	document.getElementById(id).style.display='none';
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
		  window.location.reload();
	    }
	  }
	xmlhttp.open("GET","./alterdata?query=delete&id="+id,true);
	xmlhttp.send();
}
function save(){
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
		  window.location.reload();
	    }
	  }
	var id=document.getElementById('updateid').value;
	var url=document.getElementById("updateurl").value;
	var tags=document.getElementById('updatetags').value;
	//tags=replaceAll(tags,'&','____');
	xmlhttp.open("GET","./alterdata?query=update&id="+id+"&url="+encodeURIComponent(url)+"&tags="+encodeURIComponent(tags),true);
	xmlhttp.send();
}
function replaceAll(txt, replace, with_this) {
	  return txt.replace(new RegExp(replace, 'g'),with_this);
}

$(document).ready(function(){
	$("#update_button").click(function(){
		
		//document.getElementById("modal_update").style.display='block';
		header_notification();
		var xmlhttp;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
			  window.location.reload();
			  updateRequested=false;
		    }
		  }
		if(!updateRequested){
		xmlhttp.open("GET","./updateuserdata",true);
		updateRequested=true;
		xmlhttp.send();
		}
	});
});
	
// code below for clicking on tags and opening of search results
function bindclick(){ 
$("div.tags > span").unbind("click").click(function (){
	window.open("search.jsp?tag="+encodeURIComponent($(this).text()),"_blank");
	
});
}
$(document).ready(bindclick);


$(document).ready(function(){
	$("#search").blur(function(){
		if($("#search").val()=="")
			$("#search").val("Search");
	});
	
	$("#search_button").click(function(){
		window.open("search.jsp?tag="+encodeURIComponent($("#search").val()),"_blank");
	});
	
});

$(document).ready(function(){
	$(window).scroll(function(){
		scrollPoint = ($(document).height()/2);
		if($(window).scrollTop() >= $(document).height() - $(window).height()-scrollPoint&&!listRequested){
			listRequested=true;
		  $.getJSON("./getnextlist?currentcount="+$(".row_feed:last").attr("id"),function(result){
		    $.each(result, function(i, field){
		    	
		    	var str='<div class="row_feed" id='+field.id+'><div class="edit"'+
		    	'><span><img src="images/ico_edit.png" height="20" width="20" alt="Edit Bookmark" class="button1" onclick="edit(\''+field.id+'\',\''+field.bookmark+'\',\''+field.tagsString+'\')"/></span>'
		    	+'<span><img src="images/cross4.png" height="20" width="20" alt="Delete Bookmark" class="button2" onclick="deleteShow('+field.id+')"/></span>'
		    	+'</div>'
		    	+'<div class="feed">'
		    	+'<span> <a href='+field.bookmark+' target="_blank"> '+field.bookmark.substr(0,70)+'...</a></span>'
		    	+'</div>'
		    	+'<div class="tags">';
		    	$.each(field.tags, function(j,field2){
		    		str=str+'<span>'+field2+'</span>';
		    	});
		    	str=str+'</div>';
		    	$("#container_feeds").append(str);
		    });
				bindclick();
				hoverchange();
				listRequested=false;
		  });	
		  
		}});
});

function header_notification(){
$('#header_notification').toggle();
$('#header').toggle();
setTimeout(function(){
	$('#header_notification').toggle();
	$('#header').toggle();	
	
},5000);

}

