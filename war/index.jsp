<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<jsp:include page="header.jsp" />
<title>Twitmarks</title>
<meta name="Description" content="Bookmark links in your twitter feed" />
<meta name="Keywords" content="Bookmarks Twitter" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<% if(null!=session.getAttribute("twitter")   ) { response.sendRedirect("login.jsp"); } %>
<div id="header">
<a href="#" id="logo"> Twitmarks</a>

<a href="https://docs.google.com/spreadsheet/viewform?formkey=dHo4SVNKYW0yUmpuaHloeU1jR3h0WGc6MQ" target="_blank" class= "about"> Contact Us </a>
<a href="bookmarks" class= "about"> Login </a>
</div>
<div id="wrapper">

<div id="upper_right">
<h2> Bookmark links in your Twitter feed!</h2>
<p>1. Hassle Free Bookmarking of links in Twitter feeds!</p>
<p>2. Sign in with your Twitter account!</p>
<p>3. Automated Tagging of Bookmarks!</p>
<p>4. What are you thinking? Click that button!</p>
</div>
<div id="upper_left">
<img src="images/twit.png"/>
<a href="bookmarks" id="button"> Login with Twitter </a>
</div>
</div>
<div class="container-fluid">
<div id="footer" class="row-fluid for">
<div class="span4 footer">
You don't have to go through the hassle to manually bookmark links in your twitter feed. This app would do it for you with the click of a button.
</div>
<div  class="span4 footer">
You don't need to create a separate account, just sign in with your twitter account and you are good to go.
</div>
<div class="span4 footer">
You don't have to worry about tagging bookmarks, the application would automatically extract the content of the tweet with the URL and create tags for the bookmark.
</div>
</div>
</div>
</body>
</html>