package com.bookmarks.database;
import java.util.ArrayList;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import twitter4j.Status;


import com.bookmarks.twitterapi.ExtractContentUsingTwitterApi;
import com.google.appengine.api.datastore.Key;
@PersistenceCapable
public class User {
    /**
	 * @param twitterId
	 * @param verifier
	 */
	
	@PrimaryKey
    @Persistent
	private Long twitterId;
    @Persistent
	private String token;
    @Persistent
   	private String token_secret;
    @Persistent
	private Long last_id; 
    
    public User(Long l, String token,String token_secret, Long last_id) {
		super();
		this.twitterId = l;
		this.token = token;
		this.token_secret=token_secret;
		this.last_id=last_id;
	}
	public String getToken() {
		return token;
	}
	public String getToken_Secret() {
		return token_secret;
	}
	public void setToken(String verifier) {
		this.token = verifier;
	}
	public void setToken_Secret(String verifier) {
		this.token_secret = verifier;
	}
	public long getTwitterId() {
		return twitterId;
	}
	public void setTwitterId(Long twitterId) {
		this.twitterId = twitterId;
	}
	public Long getLastId()
	{
		return this.last_id;
	}
	public void setLastId(Long id){
		this.last_id=id;
	}
	public ArrayList<Status> getTweets(){
		ArrayList<Status> tweets = ExtractContentUsingTwitterApi.Timeline(this);
		return tweets;
	}
}
