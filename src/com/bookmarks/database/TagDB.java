package com.bookmarks.database;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import java.util.ArrayList;

@PersistenceCapable
public class TagDB {
	
	@PrimaryKey
	@Persistent
	private String tag;
	
	@Persistent
	private ArrayList<String> urls=new ArrayList<String>();
	
	@Persistent
	private long twitterId;
	
	
	public TagDB(String tag,long twitterId,ArrayList<String> url){
		this.tag=tag;
		this.twitterId=twitterId;
		this.urls.addAll(url);
	}
	public TagDB(String tag,long twitterId,String url){
		this.tag=tag;
		this.twitterId=twitterId;
		this.urls.add(url);
	}
	public void setTag(String tag){
		this.tag=tag;
	}
	public void addURL(String url){
		
		this.urls.add(url);
	}
public void addURL(ArrayList<String> url){
		
		this.urls.addAll(url);
	}
	
	public void settwitterId(long twitterId){
		this.twitterId=twitterId;
	}
	
	
	public ArrayList<String> getURLs(){
		return this.urls;
	}

	

}
