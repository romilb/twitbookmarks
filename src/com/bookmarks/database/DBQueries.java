package com.bookmarks.database;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;


public class DBQueries {
	public static ArrayList<BookmarkDB> getContentforDisplay(long tweeterId, long id){
		//System.out.println("Asking content for"+ tweeterId);
		PersistenceManager pm=PMF.get().getPersistenceManager();
		//Transaction tx = (Transaction) pm.currentTransaction();
		Query query = pm.newQuery(BookmarkDB.class, "twitterId == " + tweeterId +" && id < "+id);
		query.setOrdering("id desc");
		try
		{
			//tx.begin();
			ArrayList<BookmarkDB> disp=new ArrayList<BookmarkDB>();
			ArrayList<BookmarkDB> disp2=new ArrayList<BookmarkDB>();
			Collection e = (Collection) query.execute();
			Iterator itr = e.iterator();
			int i=0;
			while(itr.hasNext() && i++<10){
				disp.add((BookmarkDB)itr.next());
			}
			//tx.commit();
			pm.close();
			//System.out.println("Time to return finally");
			return disp;
		}
		catch(Exception e){
			e.printStackTrace();
			/*if (tx.isActive())
			{
				tx.rollback();
			}*/
			pm.close();
			e.printStackTrace();
			return null;
		}
	}
	public static boolean alreadyExist(String url,long tweeterId){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		//Transaction tx = (Transaction) pm.currentTransaction();
		Query query = pm.newQuery(BookmarkDB.class, "twitterId == " + tweeterId + " && bookmark ==\"" +url +"\"");
		try
		{
			//tx.begin();
			BookmarkDB setLastId;
			Collection e = (Collection) query.execute();
			try{
				setLastId= (BookmarkDB) e.iterator().next();
				return true;
			}
			catch(NoSuchElementException ex){
				return false;
			}
			//System.out.println("Set last ID as "+setLastId.getLastId());
			finally
			{
				//tx.commit();
				pm.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			pm.close();
			return false;
		}
	}

	public static User getUserObjectfromDB(long twitterId){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		Query query = pm.newQuery(User.class, "twitterId == " + twitterId);
		Collection e=(Collection)query.execute();
		Iterator itr = e.iterator();
		User user=null;
		while(itr.hasNext()){
			user=(User)itr.next();
			pm.close();
			return user;
		}
		pm.close();
		return user;
	}
	public static void updateTagsURl(long id,String url, String[] tags, long twiterid){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		//Transaction tx = (Transaction) pm.currentTransaction();
		List<String> tags_to_save=new ArrayList<String>();
		for(String str:tags){
			str=str.replaceAll("\"", "");
			if(str==null||str.equals(""))
				continue;
			tags_to_save.add(str.trim());
		}
		Query query = pm.newQuery(BookmarkDB.class, "id == " + id + "&& twitterId ==" +twiterid);
		Collection e=(Collection)query.execute();
		try{
			BookmarkDB itr =(BookmarkDB) e.iterator().next();
			//System.out.println(tags_to_save.toString());
			//tx.begin();
			itr.setbookmark(url);
			itr.setTags(tags_to_save);
			//tx.commit();
			pm.close();
		}
		catch(Exception exx){
			//No Records found
		}
	}
	public static void deleteRecord(long id,long twiterid){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		Query query = pm.newQuery(BookmarkDB.class, "id == " + id +" && twitterId =="+twiterid);
		Collection e=(Collection)query.execute();
		try{
			BookmarkDB itr =(BookmarkDB) e.iterator().next();
			pm.deletePersistent(itr);
			pm.close();
			//System.out.println("DONE DELETING...");
		}
		catch(Exception exx){
			pm.close();
			//NO RECORD FOUND//
			//System.out.println("NO RECORD..");
		}
	}

	public static TagDB getTagObjectfromDB(long twitterId, String tag){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		tag=tag.trim();
		tag=tag.replaceAll("\"", "");
		//tag=tag.replaceAll("'", "''");
		if(tag==null||"".equals(tag))
			return null;
		//System.out.println("twitterId == " + twitterId +" && "+"tag ==\""+tag+"\"");
		Query query = pm.newQuery(TagDB.class, "twitterId == " + twitterId +" && "+"tag ==\""+tag+"\"");
		Collection e=(Collection)query.execute();
		TagDB temp_tagdb=null;
		if(!e.isEmpty()){
			temp_tagdb= (TagDB)e.iterator().next();
		}
		pm.close();
		return temp_tagdb;
	}
	
	public static void updateTagDB(String url, String[] tag,long twitterId){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		ArrayList<TagDB> list_of_tag_elements= new ArrayList<TagDB>();
		for(String each_tag:tag){
			each_tag=each_tag.trim();
			//each_tag=each_tag.replaceAll("'", "''");
			each_tag=each_tag.replaceAll("\"", "");
			if(each_tag==null || each_tag.equals(""))
				continue;
		TagDB tagdb= DBQueries.getTagObjectfromDB(twitterId,each_tag);
		if(tagdb==null){
			tagdb=new TagDB(each_tag,twitterId,url);

		}
		if(tagdb!=null){
			if(!tagdb.getURLs().contains(url))
				tagdb.addURL(url);
			
			list_of_tag_elements.add(tagdb);
			}
		}
		pm.makePersistentAll(list_of_tag_elements);
		pm.close();
		
	}
}
