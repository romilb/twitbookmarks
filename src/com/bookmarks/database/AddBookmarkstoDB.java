package com.bookmarks.database;

import javax.jdo.PersistenceManager;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Collections;
public class AddBookmarkstoDB {
	
   

    	
	public void store_data_into_db(List<String>bookmarks,long last_twit_id,long twitterId, ArrayList<ArrayList<String>>tags,List<Date> date){
	   PersistenceManager pm=PMF.get().getPersistenceManager();
		ListIterator litr=tags.listIterator();
		ListIterator getdate=date.listIterator();
		ArrayList<BookmarkDB> collection_of_bookmarks=new ArrayList<BookmarkDB>();
		for(String one_bookmark:bookmarks){
			ArrayList<String> tags_for_one_bookmark=(ArrayList<String>)litr.next();
			Date one_date=(Date) getdate.next();
			BookmarkDB bookmarksdb=new BookmarkDB(one_bookmark,twitterId,one_date);
			bookmarksdb.setTags(tags_for_one_bookmark);
			collection_of_bookmarks.add(bookmarksdb);
		}
		pm.makePersistentAll(collection_of_bookmarks);
		pm.close();
	}
	
	public void store_index_tag_data(String tag,ArrayList<String> urls, long twitterId){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		TagDB tagdb=new TagDB(tag.trim(),twitterId,urls);
		pm.makePersistent(tagdb);
		pm.close();
		
	}
	
	public void update_index_tag_data(TagDB tagdb,ArrayList<String> urls){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		tagdb.addURL(urls);
		pm.makePersistent(tagdb);
		pm.close();
	}
	

}
