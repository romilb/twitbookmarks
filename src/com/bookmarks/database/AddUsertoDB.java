/**
 * 
 */
package com.bookmarks.database;

import java.util.Collection;
import java.util.NoSuchElementException;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import com.bookmarks.backend.StoreData;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;

/**
 * @author romil
 *
 */
public class AddUsertoDB {
	public static boolean addUser(Twitter twitter,AccessToken accessToken){
		PersistenceManager pm=PMF.get().getPersistenceManager();
		User user = null;
		try {
			user = new User(twitter.getId(),accessToken.getToken(),accessToken.getTokenSecret(),1L);
			Query query = pm.newQuery(User.class,"twitterId == " + user.getTwitterId());
			Collection allProducts = (Collection)query.execute();
			Transaction tx = (Transaction) pm.currentTransaction();
			try{
				
				User iter = (User) allProducts.iterator().next();
				tx.begin();
				iter.setToken(accessToken.getToken());
				iter.setToken_Secret(accessToken.getTokenSecret());
				tx.commit();
			}
			catch(NoSuchElementException e){
				user = new User(twitter.getId(),accessToken.getToken(),accessToken.getTokenSecret(),1L);
				tx.begin();
				pm.makePersistent(user);
				tx.commit();
				//user = DBQueries.getUserObjectfromDB(twitter.getId());
				//if(user==null){
				//	pm.close();
				//	return false;
				//}
				StoreData storedata=new StoreData(user);
			}
			pm.close();
			return true;
		} catch (IllegalStateException e1) {
			// TODO Auto-generated catch block
			
			e1.printStackTrace();
			return false;
		} catch (TwitterException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
	}
	public static boolean updateSinceId(User user,Long sinceId){
		 PersistenceManager pm=PMF.get().getPersistenceManager();
		 Transaction tx = (Transaction) pm.currentTransaction();
         Query query = pm.newQuery(User.class, "twitterId == " + user.getTwitterId());

		 try
		 {
		     tx.begin();
		     User setLastId;
		     Collection e = (Collection) query.execute();
		     setLastId= (User) e.iterator().next();
		     setLastId.setLastId(sinceId);
		     //System.out.println("Set last ID as "+setLastId.getLastId());
		     tx.commit();
		     pm.close();
		 }
		 catch (Exception e)
		 {
		     if (tx.isActive())
		     {
		         tx.rollback();
		     }
		     pm.close();
		     return false;
		 }
		 return true;
	}
}
