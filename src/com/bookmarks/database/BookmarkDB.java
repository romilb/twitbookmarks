package com.bookmarks.database;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Date;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.bookmarks.backend.CleanTags;

import twitter4j.JSONObject;



//import com.google.appengine.repackaged.org.json.JSONObject;

//import net.sf.json.*;

@PersistenceCapable
public class BookmarkDB {

	@PrimaryKey  
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long id;
	@Persistent
	private String bookmark;
	@Persistent
	private Date date;
	@Persistent
	private long twitterId;
	@Persistent
	private ArrayList<String> tags=new ArrayList<String>();
	public BookmarkDB(){

	}

	public BookmarkDB(String one_bookmark, long twitterId, Date date){
		this.twitterId=twitterId;
		this.date= date;
		this.bookmark=one_bookmark;
	}
	public void setTags(List<String>tags_to_save){
		this.tags.clear();
		for(String str:tags_to_save)
			this.tags.add(str);
		String[] new_tags = CleanTags.getCleanedTag(this.tags.toArray(new String[this.tags.size()]));
		ArrayList<String> ntags = new ArrayList<String>();
		for(String s:new_tags){
			ntags.add(s);
		}
		this.tags = ntags;
	}
	
	
	public List<String> getTags(){
		String[] new_tags = CleanTags.getCleanedTag(this.tags.toArray(new String[this.tags.size()]));
		ArrayList<String> ntags = new ArrayList<String>();
		for(String s:new_tags){
			ntags.add(s);
		}
		this.tags = ntags;
		return this.tags;
	}
	public List<String> getEscapedTags(){
		ArrayList <String> tags_escaped=new ArrayList <String>();
		for(String temp_tag:this.tags){
			tags_escaped.add(temp_tag.replace("'", "\\'"));
		}
		
		return tags_escaped;
	}
	
	
	
	public String getbookmark(){
		return this.bookmark;
	}
	public long getId(){
		return this.id;
	}
	public void setbookmark(String url){
		this.bookmark=url;
	}
	public static String extract(String url) {

		URL url2;
		//return url;
		try {
			url2 = new URL("http://urltoshorturl.appspot.com/expand?url="+url);
			HttpURLConnection connection = (HttpURLConnection)url2.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String str,fin="";
			while ((str = in.readLine()) != null) {
				// str is one line of text; readLine() strips the newline character(s)
				fin+=str;
			}
			//System.out.println(fin);
			JSONObject json= new JSONObject(fin);
			return json.getString("end_url");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return url;
		}
	}
	public static void main(String args[]) throws Exception{
		System.out.println(extract("http://t.co/UtV8tsA7"));
	}
}
