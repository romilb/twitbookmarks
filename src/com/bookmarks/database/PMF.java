/**
 * 
 */
package com.bookmarks.database;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;

/**
 * @author romil
 *
 */
public final class PMF {

	private static final PersistenceManagerFactory pmfinstance=JDOHelper.getPersistenceManagerFactory("transactions-optional");
	public PMF() {
		// TODO Auto-generated constructor stub
	}
	public static PersistenceManagerFactory get() {
		return pmfinstance;
	}

}
