package com.bookmarks.twitterapi;

import java.util.ArrayList;

import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import com.bookmarks.database.AddUsertoDB;
import com.bookmarks.database.User;

public class ExtractContentUsingTwitterApi {
	public static ArrayList<Status> Timeline(User user) {
		//System.out.println("Last Id="+user.getLastId());
		long sinceId=user.getLastId();
		Paging page=new Paging(sinceId);
		page.setCount(20);
		Twitter twitter =new TwitterFactory().getInstance();
		//twitter.setOAuthConsumer("VlE1BARB8hmrCR0nvNrI9A", "YaJ4FAc8kFWkHk4X8eTkIehUb4B2yznZ5xRooUMDHc");
		twitter.setOAuthAccessToken(new AccessToken(user.getToken(),user.getToken_Secret()));
		 ArrayList<Status> statuses;
		try {
			statuses = ((ArrayList<Status>) twitter.getHomeTimeline(page));
			if(!statuses.isEmpty())
			 AddUsertoDB.updateSinceId(user, statuses.get(0).getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ArrayList<Status>();
		}
		 System.out.println("GOT THIS NO of STATUS="+statuses.size());
		 //---------------------Trying Update----------
		 //System.out.println(statuses);
		 //--------------------------------------------
		return statuses;
		
	}
}
