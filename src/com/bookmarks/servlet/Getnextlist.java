package com.bookmarks.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.tagext.TagSupport;

import twitter4j.JSONArray;
import twitter4j.JSONObject;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import com.bookmarks.database.BookmarkDB;
import com.bookmarks.database.DBQueries;

public class Getnextlist extends HttpServlet{

	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
		Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
        long curr = Long.parseLong(request.getParameter("currentcount"));
		JSONObject json= new JSONObject();
		JSONArray array=new JSONArray();
		try {
			List<BookmarkDB> forDisplay=DBQueries.getContentforDisplay(twitter.getId(),curr);
			 for(int i=0;i<forDisplay.size();i++){
				 BookmarkDB books=forDisplay.get(i);
				 JSONObject obj= new JSONObject();
				 JSONArray arr=new JSONArray();
				 obj.put("id", books.getId());
				 obj.put("bookmark", books.getbookmark());
				 obj.put("tagsString", books.getEscapedTags().toString().substring(1,books.getEscapedTags().toString().length()-1));
				 for(String s:books.getTags()){
					 arr.put(s);
				 }
				 obj.put("tags", arr);
				 array.put(obj);
			 }
			 json.put("list", array);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(array.toString());
}
}