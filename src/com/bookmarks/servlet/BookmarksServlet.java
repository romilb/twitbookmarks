package com.bookmarks.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;

@SuppressWarnings("serial")
public class BookmarksServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException,ServletException {
		Twitter twitter = new TwitterFactory().getInstance();
		//twitter.setOAuthConsumer("VlE1BARB8hmrCR0nvNrI9A", "YaJ4FAc8kFWkHk4X8eTkIehUb4B2yznZ5xRooUMDHc");
        request.getSession().setAttribute("twitter", twitter);
        try {
            StringBuffer callbackURL = request.getRequestURL();
            int index = callbackURL.lastIndexOf("/");
            callbackURL.replace(index, callbackURL.length(), "").append("/callback");
            RequestToken requestToken = twitter.getOAuthRequestToken(callbackURL.toString());
            request.getSession().setAttribute("requestToken", requestToken);
            response.sendRedirect(requestToken.getAuthenticationURL());
        } catch (TwitterException e) {
            throw new ServletException(e);
        }
	}
}
