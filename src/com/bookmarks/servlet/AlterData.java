/**
 * 
 */
package com.bookmarks.servlet;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.bookmarks.backend.CleanTags;
import com.bookmarks.database.DBQueries;

import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * @author romil
 *
 */
public class AlterData extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException,ServletException {
		Twitter twitter=(Twitter) request.getSession().getAttribute("twitter");
		String query=request.getParameter("query");
		long id=Long.parseLong(request.getParameter("id"));
		if(query.equals("update")){
			String url=request.getParameter("url");
			String tags=request.getParameter("tags");
			//tags=tags.replaceAll("____", "&");
			url=URLDecoder.decode(url, "UTF-8");
			tags=URLDecoder.decode(tags, "UTF-8");
			String[] tag=tags.split(",");
			tag=CleanTags.getCleanedTag(tag);
			try {
				DBQueries.updateTagDB(url,tag,twitter.getId());
				DBQueries.updateTagsURl(id,url,tag,twitter.getId());
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(query.equals("delete"))
		{
			//System.out.println("id="+id);
			try {
				DBQueries.deleteRecord(id,twitter.getId());
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
