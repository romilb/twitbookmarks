package com.bookmarks.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookmarks.backend.StoreData;
import com.bookmarks.database.DBQueries;
import com.bookmarks.database.User;

import twitter4j.Twitter;
import twitter4j.TwitterException;

public class UpdateUserData extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException,ServletException {
		Twitter twitter=(Twitter) request.getSession().getAttribute("twitter");
		User user = null;
		try {
			user = DBQueries.getUserObjectfromDB(twitter.getId());
			if(user==null)
				throw new IllegalStateException();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StoreData storedata=new StoreData(user);
		response.sendRedirect("login.jsp");
		
	}
}
