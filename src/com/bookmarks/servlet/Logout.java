package com.bookmarks.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import twitter4j.Twitter;

public class Logout extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	request.getSession().removeAttribute("twitter");
    	request.getSession().invalidate();
    	//System.out.println("Y"+request.getRequestURL());
    	response.sendRedirect(request.getRequestURL().substring(0,request.getRequestURL().toString().lastIndexOf('/')));
    }
}
