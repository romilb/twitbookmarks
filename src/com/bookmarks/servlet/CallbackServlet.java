package com.bookmarks.servlet;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.bookmarks.database.AddUsertoDB;
import java.io.IOException;

public class CallbackServlet extends HttpServlet {
    private static final long serialVersionUID = 1657390011452788111L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        try {
        	Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
            RequestToken requestToken = (RequestToken) request.getSession().getAttribute("requestToken");
            String verifier = request.getParameter("oauth_verifier");
            AccessToken accessToken=twitter.getOAuthAccessToken(requestToken, verifier);           
            request.getSession().removeAttribute("requestToken");
            twitter.setOAuthAccessToken(accessToken);
            
            //=======DATASTORE TEST==============//
            boolean status = AddUsertoDB.addUser(twitter,accessToken);
            //=========END DATASTORE CODE=============//
       
            if(!status){
            	throw new TwitterException("Error");
            }
        } catch (Exception e) {
        	//request.getSession().invalidate();
        	response.sendRedirect(request.getContextPath() + "/bookmarks");
            //throw new ServletException(e);
        }
        response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
}