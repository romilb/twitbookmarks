package com.bookmarks.backend;

import java.util.ArrayList;
import java.util.Arrays;

import opennlp.tools.postag.POSTaggerME;


public class ExtractHashTagsafterNLP {

	private static final POSTaggerME tagger= new TrainingModel().getTrainedModel();	
	
	public static ArrayList<String> gettags(String tweet){
		String tokens[]=tweet.split(" ");
        ArrayList<String>tags=new ArrayList<String>();
		try {
		  String temp_tags[]=tagger.tag(tokens);
		  for(int i=0;i<temp_tags.length;i++){
			  if(temp_tags[i].contains("NN")&&!(tokens[i].contains("http"))&&!(tokens[i].trim().equals("")))
			  	tags.add(tokens[i].trim().replaceAll("\"", ""));
		  }
		  
		  String[] clean_tags=(String[])tags.toArray(new String[1]);
		  clean_tags=CleanTags.getCleanedTag(clean_tags);
		  tags= new ArrayList(Arrays.asList(clean_tags));
		}
		catch (Exception e) {
		  // Model loading failed, handle the error
		  e.printStackTrace();
		}
		return tags;
	}
	public static void main(String args[]){
		String line="http://abc is on fire";
		System.out.print(ExtractHashTagsafterNLP.gettags(line).toString()); 
	}


}
