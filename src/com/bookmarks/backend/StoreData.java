package com.bookmarks.backend;
import com.bookmarks.database.*;
import java.util.ListIterator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;

import twitter4j.Status;
public class StoreData {

	private User user;
	private List<Status> tweets;
	private ArrayList <ArrayList<String>>tags = new ArrayList <ArrayList<String>>(); 
	private ArrayList <ArrayList<String>>urls= new ArrayList<ArrayList<String>>();
	public StoreData(User user){
		//System.out.println(user);
		//System.out.println(user.getTwitterId());
		this.user=user;
		tweets=user.getTweets();
		initializetags();
		initilizeURLs();
		StoreDataintoDb();
		store_indexed_tags_into_db();
	}

	public void initializetags(){
		for(Status tweet:tweets){
			tags.add(ExtractHashTagsafterNLP.gettags(tweet.getText()));
		}
	}

	public void initilizeURLs(){
		for(Status tweet:tweets){
			urls.add(ExtractURL.getURL(tweet));
		}
	}


	public void StoreDataintoDb(){
		AddBookmarkstoDB addbookmarkdb=new AddBookmarkstoDB();
		List<Status>tweets=new ArrayList<Status>();
		List<Date> dates=new ArrayList<Date>();
		List<String>bookmarks=new ArrayList<String>();
		ArrayList<ArrayList<String>>tags_of_all_tweets=new ArrayList <ArrayList<String>>();
		int tweet_size=this.tweets.size();
		for(int i=0;i<tweet_size;i++){

			ArrayList<String> tags_temp=this.tags.get(i);
			for(String url:this.urls.get(i)){
				if(url==null || "".equals(url)||DBQueries.alreadyExist(url,this.user.getTwitterId()))
					continue;
				tags_of_all_tweets.add(tags_temp);
				bookmarks.add(url);
				dates.add(this.tweets.get(i).getCreatedAt());
			}
		}		

		Long last_twit_id=this.user.getLastId();
		addbookmarkdb.store_data_into_db(bookmarks,last_twit_id,this.user.getTwitterId(), tags_of_all_tweets,dates );	
	}

	public void store_indexed_tags_into_db(){

		AddBookmarkstoDB addbookmarkdb=new AddBookmarkstoDB();

		HashMap<String,ArrayList<String>> tags_hashmap =new HashMap<String,ArrayList<String>> ();
		int urls_size=this.urls.size();
		for(int i=0;i<urls_size;i++){
			ArrayList<String> url_for_one_tweet=this.urls.get(i);
			ArrayList<String> tag_for_one_tweet=this.tags.get(i);
			int tag_for_one_tweet_size=tag_for_one_tweet.size();
			for(int j=0;j<tag_for_one_tweet_size;j++){
				int url_for_one_tweet_size=url_for_one_tweet.size();
				String temp_tag=tag_for_one_tweet.get(j);
				temp_tag=temp_tag.trim();
				if(temp_tag==null||temp_tag.equals(""))
					continue;
				for(int k=0;k<url_for_one_tweet_size;k++){
					String temp_url=url_for_one_tweet.get(k);
					if(tags_hashmap.containsKey(temp_tag)){
						ArrayList<String >temp= tags_hashmap.get(temp_tag);
						temp.add(temp_url);
						tags_hashmap.put(temp_tag, temp);
					}else{
						ArrayList<String >temp= new ArrayList<String>();
						temp.add(temp_url);
						tags_hashmap.put(temp_tag, temp);
					}
				}
			}
		}

		for(String key:tags_hashmap.keySet()){
			TagDB tagdb=DBQueries.getTagObjectfromDB(this.user.getTwitterId(),key);
			if(tagdb==null){
				addbookmarkdb.store_index_tag_data(key,tags_hashmap.get(key), this.user.getTwitterId());
			}else{
				addbookmarkdb.update_index_tag_data(tagdb,tags_hashmap.get(key));
			}
		}
	}
}
