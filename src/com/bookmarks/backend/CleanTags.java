package com.bookmarks.backend;

import java.util.ArrayList;

public class CleanTags {
	
	public static String[] getCleanedTag(String[] tag){
		tag=removeWhiteSpace(tag);
		tag=toLowerCase(tag);
		tag=removeCommaorAppostoporDot(tag);
		tag=removeUsers(tag);
		tag=removeWhiteSpace(tag);
		return tag;
	}
	
	public static String[] removeWhiteSpace(String tag[]){
		for(int i=0;i<tag.length;i++){
			tag[i]=tag[i].trim();
		}
		
		return tag;
	}
	
	public static String[] removeCommaorAppostoporDot(String[] tag){
		for(int i=0;i<tag.length;i++){
			tag[i]=tag[i].replaceAll(",", " ");
			tag[i]=tag[i].replaceAll("'s", "");
			tag[i]=tag[i].replaceAll("\\.", "");
			tag[i]=tag[i].replaceAll(":", " ");
			tag[i]=tag[i].replaceAll(";", " ");
			tag[i]=tag[i].replaceAll("[()#]", "");
		}
		return tag;
	}
	
	public static String[] removeUsers(String[] tag){
		ArrayList<String> al = new ArrayList<String>();
		for(String i:tag){
			if(i.startsWith("@") || i.startsWith("rt") || i.length()<3){
				continue;
			}
			else{
				al.add(i);
			}
		}
		String[] newTags = new String[al.size()];
		for(int i=0;i<al.size();i++){
			newTags[i] = al.get(i);
		}
		return newTags;
	}
	
	public static String[] toLowerCase(String[] tag){
		for(int i=0;i<tag.length;i++){
			tag[i]=tag[i].toLowerCase();
			
		}
		return tag;
	}
	
	public static void main(String args[]){
		String[] tags={"gaurav,","GauRav", "gaurav....", "Gaurav's"};
		tags=CleanTags.getCleanedTag(tags);
		for(String tag:tags)
		System.out.println(tag);
	}

}
