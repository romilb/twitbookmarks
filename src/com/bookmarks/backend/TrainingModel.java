package com.bookmarks.backend;


import java.io.FileInputStream;
import java.io.InputStream;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

public class TrainingModel {


	public POSTaggerME getTrainedModel(){
		InputStream modelIn;
		POSTaggerME tagger=null;
		POSModel model;
		try{
			
			modelIn=getClass().getResourceAsStream("/resources/en-pos-maxent.bin");
			model = new POSModel(modelIn);
		    tagger = new POSTaggerME(model);

		}catch (Exception e){
			e.printStackTrace();
		}
		return tagger;
	}
	
}
