package com.bookmarks.backend;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Pattern;

import twitter4j.Status;
import twitter4j.URLEntity;

import com.bookmarks.database.BookmarkDB;
public  class ExtractURL {

	
	static Pattern pattern=Pattern.compile("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
	public static ArrayList<String> getURL(Status tweet){
		ArrayList<String> url=new ArrayList<String>();
		//System.out.println(tweet.getURLEntities());
		/*Matcher matcher=pattern.matcher(tweet);
		while(matcher.find()){
			url.add(matcher.group());
			
		}*/
		HashSet<String> urls=new HashSet<String>();
		for(URLEntity str:tweet.getURLEntities()){
			urls.add(BookmarkDB.extract(str.getURL()));
		}
	return new ArrayList<String>(urls);	
	}
	
	public static void main(String args[]){
		//System.out.println(ExtractURL.getURL("a http://www.google.com is a very good https://facebook.com website"));
	}
	
	
}
